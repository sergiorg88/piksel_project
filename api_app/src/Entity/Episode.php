<?php

namespace App\Entity;

use App\Repository\EpisodeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: EpisodeRepository::class)]
class Episode
{
    #[ORM\Id]
    #[ORM\Column(type: 'guid')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $name;

    #[ORM\ManyToOne(targetEntity: Studio::class, inversedBy: 'episodes')]
    #[ORM\JoinColumn(nullable: false)]
    #[Ignore]
    private $rightsowner;

    #[ORM\OneToMany(mappedBy: 'episode', targetEntity: RoyaltyManager::class)]
    #[Ignore]
    private $royaltyManagers;

    public function __construct(string $id)
    {
        $this->id = $id;
        $this->royaltyManagers = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getRightsowner(): ?Studio
    {
        return $this->rightsowner;
    }

    public function setRightsowner(?Studio $rightsowner): self
    {
        $this->rightsowner = $rightsowner;

        return $this;
    }

    /**
     * @return Collection<int, RoyaltyManager>
     */
    public function getRoyaltyManagers(): Collection
    {
        return $this->royaltyManagers;
    }

    public function addRoyaltyManager(RoyaltyManager $royaltyManager): self
    {
        if (!$this->royaltyManagers->contains($royaltyManager)) {
            $this->royaltyManagers[] = $royaltyManager;
            $royaltyManager->setEpisode($this);
        }

        return $this;
    }

    public function removeRoyaltyManager(RoyaltyManager $royaltyManager): self
    {
        if ($this->royaltyManagers->removeElement($royaltyManager)) {
            // set the owning side to null (unless already changed)
            if ($royaltyManager->getEpisode() === $this) {
                $royaltyManager->setEpisode(null);
            }
        }

        return $this;
    }

}
