<?php

namespace App\DataFixtures;

use App\Entity\Episode;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class EpisodeFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $episodes_json = file_get_contents("./data/episodes.json");
        $aux = json_decode($episodes_json, true);
        $episodes = array_shift($aux);
      
        foreach ($episodes as $episode) {
          $ep = new Episode($episode["id"]);
          $ep->setName($episode["name"]);
          $ep->setRightsowner($this->getReference("studio".$episode["rightsowner"]));

          $manager->persist($ep);
        }

        $manager->flush();
    }

    public function getOrder(): int
    {
        return 2;
    }
}
