<?php

namespace App\DataFixtures;

use App\Entity\Studio;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class StudioFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        
        $studios_json = file_get_contents("./data/studios.json");
        $aux = json_decode($studios_json, true);
        $studios = array_shift($aux);
       
        foreach ($studios as $studio) {
          $st = new Studio($studio["id"]);
          $st->setName($studio["name"]);
          $st->setPayment($studio["payment"]);
          $this->addReference("studio".$studio["id"], $st);

          $manager->persist($st);
        }

        $manager->flush();
    }

    public function getOrder(): int
    {
        return 1;
    }
}
