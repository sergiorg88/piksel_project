<?php

namespace App\Controller;

use App\Repository\EpisodeRepository;
use App\Repository\RoyaltyManagerRepository;
use App\Repository\StudioRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class RoyaltyManagerController extends AbstractController
{
    private $serializer;

    public function __construct(private RoyaltyManagerRepository $royaltymanagerRepository, private EpisodeRepository $episodeRepository,
    private StudioRepository $studioRepository)
    {
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $this->serializer = new Serializer($normalizers, $encoders);
    }

    #[Route("royaltymanager/payments", name:"all payments by rightowners", methods:["GET"])]
    function getPayments(): Response
    {
        $data = $this->royaltymanagerRepository->payments();

        if ($data) {
            return $this->json($data);
        } else {
            return $this->json([]);
        }
    }

    #[Route("royaltymanager/payments/{id}", name:"get payments from a rightowner", methods:["GET"])]
    function getPaymentsByRightOwnerId(String $id): Response
    {
        $studio = $this->studioRepository->findOneBy(["id" => $id]);

        if ($studio) {
            $data = $this->royaltymanagerRepository->paymentsByRightOwner($studio);
            return $this->json($data);
        } else {
            return $this->json(["error" => "GUID not found"], 404);
        }
    }

    #[Route("royaltymanager/viewing", name: "create new viewing", methods: ["POST"])]
    public function create(Request $request): Response
    {
        $data = json_decode($request->getContent(), true);

        if(isset($data['episode']) && isset($data['customer'])) {
            $episode = $this->episodeRepository->findOneBy(["id" => $data['episode']]);

            if(isset($episode)) {
                $this->royaltymanagerRepository->save($episode, $data['customer']);

                return $this->json([], 202);
            } else {
                return $this->json(["error" => "Episode not found"], 404);
            }
        } else {
            return $this->json(["error" => "Expecting mandatory parameters"], 404);
        }
    }

    #[Route("royaltymanager/reset", name: "reset royalty manager", methods: ["POST"])]
    public function reset(): Response
    {
        $this->royaltymanagerRepository->resetRoyaltyManager();
        return $this->json([], 202);
    }

}
