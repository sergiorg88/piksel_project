<?php

namespace App\Controller;

use App\Repository\StudioRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class StudioController extends AbstractController
{
    private $serializer;
    private $studioRepository;

    public function __construct(StudioRepository $studioRepository)
    {
        $this->studioRepository = $studioRepository;

        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $this->serializer = new Serializer($normalizers, $encoders);
    }

    #[Route("/studios", name:"all studios", methods:["GET"])]
    function all(): Response
    {
        $data = $this->studioRepository->findAll();

        $jsonContent = $this->serializer->serialize($data, 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            },
        ]);

        return $this->json(json_decode($jsonContent));
    }

    #[Route("studios/{id}", name:"get studio by id", methods:["GET"])]
    function getById(String $id): Response
    {
        $data = $this->studioRepository->findOneBy(["id" => $id]);

        if ($data) {
            $jsonContent = $this->serializer->serialize($data, 'json', [
                'circular_reference_handler' => function ($object) {
                    return $object->getId();
                },
            ]);

            return $this->json(json_decode($jsonContent));
        } else {
            return $this->json(["error" => "Studio not found"], 404);
        }
    }
}
