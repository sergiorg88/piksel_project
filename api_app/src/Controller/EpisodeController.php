<?php

namespace App\Controller;

use App\Repository\EpisodeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class EpisodeController extends AbstractController
{
    private $serializer;
    private $episodeRepository;

    public function __construct(EpisodeRepository $episodeRepository)
    {
        $this->episodeRepository = $episodeRepository;

        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $this->serializer = new Serializer($normalizers, $encoders);
    }

    #[Route("episodes", name:"all episodes", methods:["GET"])]
    function all(): Response
    {
        $data = $this->episodeRepository->findAll();

        $jsonContent = $this->serializer->serialize($data, 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            },
        ]);

        return $this->json(json_decode($jsonContent));
    }

    #[Route("episodes/{id}", name:"get episode by id", methods:["GET"])]
    function getById(String $id): Response
    {
        $data = $this->episodeRepository->findOneBy(["id" => $id]);

        if ($data) {
            $jsonContent = $this->serializer->serialize($data, 'json', [
                'circular_reference_handler' => function ($object) {
                    return $object->getId();
                },
            ]);

            return $this->json(json_decode($jsonContent));
        } else {
            return $this->json(["error" => "Episode not found"], 404);
        }
    }
}
