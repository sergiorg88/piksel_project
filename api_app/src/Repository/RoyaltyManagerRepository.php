<?php

namespace App\Repository;

use App\Entity\Episode;
use App\Entity\RoyaltyManager;
use App\Entity\Studio;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @method RoyaltyManager|null find($id, $lockMode = null, $lockVersion = null)
 * @method RoyaltyManager|null findOneBy(array $criteria, array $orderBy = null)
 * @method RoyaltyManager[]    findAll()
 * @method RoyaltyManager[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RoyaltyManagerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, private EntityManagerInterface $manager)
    {
        parent::__construct($registry, RoyaltyManager::class);
    }

    public function save(Episode $episode, string $customer) {
        $newViewing = new RoyaltyManager();

        $newViewing
            ->setEpisode($episode)
            ->setCustomerId($customer)
            ->setCreatedAt(new \DateTimeImmutable());
        
        $this->manager->persist($newViewing);
        $this->manager->flush();
    }

    public function payments() {
        $query = $this->manager->createQuery('
            SELECT st.id as rightsownerid, st.name as rightsowner, count(rm.id)*st.payment as royalty, count(rm.id) as viewings
            FROM App\Entity\RoyaltyManager rm
            LEFT JOIN rm.episode e
            LEFT JOIN e.rightsowner st
            GROUP BY st.id
        ');

        return $query->getResult();
    }

    public function paymentsByRightOwner(Studio $rightowner) {
        $query = $this->manager->createQuery('
            SELECT st.name as rightsowner, count(rm.id)*st.payment as royalty, count(rm.id) as viewings
            FROM App\Entity\RoyaltyManager rm
            LEFT JOIN rm.episode e
            LEFT JOIN e.rightsowner st
            WHERE st.id = :rightowner_id
            GROUP BY st.id
        ')->setParameter('rightowner_id', $rightowner);

        return $query->getResult();
    }

    public function resetRoyaltyManager() {
        $classMetaData = $this->manager->getClassMetadata('App\Entity\RoyaltyManager');
        $conn = $this->manager->getConnection();
        $dbPlatform = $conn->getDatabasePlatform();

        $conn->query('SET FOREIGN_KEY_CHECKS=0');
        $q = $dbPlatform->getTruncateTableSql($classMetaData->getTableName());
        $conn->executeUpdate($q);
        $conn->query('SET FOREIGN_KEY_CHECKS=1');
    }

}
