<?php

namespace App\Tests\API;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\RoyaltyManager;
use Faker\Factory;

class RoyaltyManagerControllerTest extends ApiTestCase
{
    public function testCreateViewing(): void
    {
        $generator = Factory::create("es_ES");
        $customer_uuid = $generator->uuid;

        $client = static::createClient();
        $client->request('POST', '/royaltymanager/viewing', ['json' => [
            'episode' => 'd5ca9218562a4c94bdca9955cec2870e',
            'customer' => $customer_uuid
        ]]);

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(202);
        $this->assertResponseHeaderSame('content-type', 'application/json');

        $client->request('POST', '/royaltymanager/viewing', ['json' => [
            'episode' => '6a1db5d6610a4c048d3df9a6268c68dc',
            'customer' => $customer_uuid
        ]]);

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(202);
        $this->assertResponseHeaderSame('content-type', 'application/json');
    }

    public function testCreateInvalidViewing(): void
    {
        $generator = Factory::create("es_ES");
        $customer_uuid = $generator->uuid;

        $client = static::createClient();
        $client->request('POST', '/royaltymanager/viewing', ['json' => [
            'episode' => 'd5ca9218562a4c94bdca',
            'customer' => $customer_uuid
        ]]);

        $this->assertResponseStatusCodeSame(404);
        $this->assertResponseHeaderSame('content-type', 'application/json');
        $response = $client->getResponse()->getbrowserKitResponse()->getContent();
        $this->assertArrayHasKey('error', json_decode($response, true));
        $this->assertContains('Episode not found', json_decode($response, true));

        $client = static::createClient();
        $client->request('POST', '/royaltymanager/viewing', ['json' => [
            'customer' => $customer_uuid
        ]]);

        $this->assertResponseStatusCodeSame(404);
        $this->assertResponseHeaderSame('content-type', 'application/json');
        $response = $client->getResponse()->getbrowserKitResponse()->getContent();
        $this->assertArrayHasKey('error', json_decode($response, true));
        $this->assertContains('Expecting mandatory parameters', json_decode($response, true));
    }

    public function testGetCollection(): void
    {
        $client = static::createClient();

        $client->request('GET', '/royaltymanager/payments');
        $this->assertResponseIsSuccessful();

        $this->assertResponseHeaderSame('content-type', 'application/json');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertCount(2, json_decode($client->getResponse()->getContent(), true));

        $client->request('GET', '/payments');
        $this->assertEquals(404, $client->getResponse()->getStatusCode());
    }

    public function testGetRoyaltyManager(): void
    {
        $client = static::createClient();

        $client->request('GET', '/royaltymanager/payments/665115721c6f44e49be3bd3e26606026');
        $this->assertResponseIsSuccessful();

        $this->assertResponseHeaderSame('content-type', 'application/json');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $response_array = json_decode($client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('rightsowner', $response_array[0]);
        $this->assertArrayHasKey('royalty', $response_array[0]);
        $this->assertArrayHasKey('viewings', $response_array[0]);
        $this->assertEquals(12, $response_array[0]['royalty']);
    }

    public function testReset(): void
    {
        dump('Reset royaltymanager table via API test');
        $client = static::createClient();
        $client->request('POST', '/royaltymanager/reset');      
        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(202);
        $this->assertCount(0, json_decode($client->getResponse()->getContent(), true));

    }
}
