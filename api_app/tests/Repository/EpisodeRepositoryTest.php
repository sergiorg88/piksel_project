<?php

namespace App\Tests\Repository;

use App\Entity\Episode;
use App\Repository\EpisodeRepository;
use App\Repository\StudioRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use App\DataFixtures\EpisodeFixtures;
use App\DataFixtures\StudioFixtures;
use Faker\Factory;

class EpisodeRepositoryTest extends KernelTestCase
{
    private EntityManagerInterface $entityManager;
    private StudioRepository $studioRepository;
    private EpisodeRepository $episodeRepository;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();
        $this->assertSame('test', $kernel->getEnvironment());
        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

            $loader = new Loader();
            $loader->addFixture(new StudioFixtures);
            $loader->addFixture(new EpisodeFixtures);
    
        $purger = new ORMPurger($this->entityManager);
        $executor = new ORMExecutor($this->entityManager, $purger);
        $executor->execute($loader->getFixtures());

        $container = static::getContainer();
        $this->studioRepository = $container->get(StudioRepository::class);
        $this->episodeRepository = $container->get(EpisodeRepository::class);
        parent::setUp();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        $this->entityManager->close();
    }

    public function testEpisodeRepository(): void
    {
        $generator = Factory::create("es_ES");
        $studios = $this->studioRepository->findAll();

        $episode = $this->episodeRepository->find('6a1db5d6610a4c048d3df9a6268c68dc');
        $this->assertEquals('Game of Thrones S1:E1', $episode->getName());

        $episode = $this->episodeRepository->findOneBy(['name' => 'The Wire S2:E4']);
        $this->assertEquals('269c71ebfa44448ba9bfdfa7e5332def', $episode->getId());

        $episode = $this->episodeRepository->findOneBy(['id' => '907d2138009d471a9a3b7ce68c3f032d']);
        $this->assertEquals('A League of Their Own S3:E5', $episode->getName());

        $new_name = $generator->name;
        $episode->setName($new_name . ' Rename');

        $this->assertEquals($new_name . ' Rename', $episode->getName());

        for ($i=0; $i < 2; $i++) { 
            $guid = $generator->uuid;
            $name = $generator->streetAddress;
            shuffle($studios);

            $episode = new Episode($guid);
            $episode->setName($name);
            $episode->setRightsowner($studios[0]);

            $this->entityManager->persist($episode);
            $this->entityManager->flush();
            $this->assertNotNull($episode->getId());

            $byId = $this->episodeRepository->findOneBy(["id" => $episode->getId()]);
            $this->assertEquals($name, $byId->getName());
            $this->assertEquals($studios[0], $byId->getRightsowner());
        }        
    }
}
