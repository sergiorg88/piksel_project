<?php

namespace App\Tests\Repository;

use App\Repository\EpisodeRepository;
use App\Repository\RoyaltyManagerRepository;
use App\Repository\StudioRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Faker\Factory;

class RoyaltyManagerRepositoryTest extends KernelTestCase
{
    private EntityManagerInterface $entityManager;
    private StudioRepository $studioRepository;
    private EpisodeRepository $episodeRepository;
    private RoyaltyManagerRepository $royaltyManagerRepository;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();
        $this->assertSame('test', $kernel->getEnvironment());
        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $purger = new ORMPurger($this->entityManager);
        $executor = new ORMExecutor($this->entityManager, $purger);

        $container = static::getContainer();
        $this->studioRepository = $container->get(StudioRepository::class);
        $this->episodeRepository = $container->get(EpisodeRepository::class);
        $this->royaltyManagerRepository = $container->get(RoyaltyManagerRepository::class);

        parent::setUp();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        $this->entityManager->close();
    }

    public function testRoyaltyManagerRepository(): void
    {
        $generator = Factory::create("es_ES");
        $customer_uuid = $generator->uuid;
        $episode = $this->episodeRepository->find('6a1db5d6610a4c048d3df9a6268c68dc');      
        $this->royaltyManagerRepository->save($episode, $customer_uuid);

        $royalty = $this->royaltyManagerRepository->findOneBy(['episode' => $episode]);
        $this->assertEquals($episode, $royalty->getEpisode());

        $customer_uuid = $generator->uuid;
        $episode = $this->episodeRepository->find('25284b37846e4b8fa17fdceaf992237e');      
        $this->royaltyManagerRepository->save($episode, $customer_uuid);

        $royalty = $this->royaltyManagerRepository->findOneBy(['episode' => $episode]);
        $this->assertEquals($episode, $royalty->getEpisode());

        $studio = $this->studioRepository->findOneBy(['name' => 'HBO']);
        $data = $this->royaltyManagerRepository->paymentsByRightOwner($studio);

        $customer_uuid = $generator->uuid;
        $episode = $this->episodeRepository->find('8c67dbe173ff42f28f5e2116c111ec7a');      
        $this->royaltyManagerRepository->save($episode, $customer_uuid);

        $data = $this->royaltyManagerRepository->paymentsByRightOwner($studio);
        $this->assertEquals(24, $data[0]['royalty']);
        $this->assertEquals(2, $data[0]['viewings']);

        $data = $this->royaltyManagerRepository->payments();
        $this->assertEquals(2, count($data));

        dump('Reset royaltymanager table via repository test');
        $this->royaltyManagerRepository->resetRoyaltyManager();
        $data = $this->royaltyManagerRepository->payments();
        $this->assertEquals(0, count($data));

    }
}
