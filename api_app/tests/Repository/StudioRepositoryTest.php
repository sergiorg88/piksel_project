<?php

namespace App\Tests\Repository;

use App\Entity\Studio;
use App\Repository\StudioRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use App\DataFixtures\StudioFixtures;
use Faker\Factory;

class StudioRepositoryTest extends KernelTestCase
{
    private EntityManagerInterface $entityManager;
    private StudioRepository $studioRepository;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();
        $this->assertSame('test', $kernel->getEnvironment());
        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $loader = new Loader();
        $loader->addFixture(new StudioFixtures);

        $purger = new ORMPurger($this->entityManager);
        $executor = new ORMExecutor($this->entityManager, $purger);
        $executor->execute($loader->getFixtures());

        $container = static::getContainer();
        $this->studioRepository = $container->get(StudioRepository::class);

        parent::setUp();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        $this->entityManager->close();
    }

    public function testStudioRepository(): void
    {
        $studio = $this->studioRepository->find('665115721c6f44e49be3bd3e26606026');
        $this->assertEquals('HBO', $studio->getName());
        $this->assertEquals(12, $studio->getPayment());

        $studio = $this->studioRepository->findOneBy(['name' => 'FOX']);
        $this->assertEquals('49924ec6ec6c4efca4aa8b0779c89406', $studio->getId());
        $this->assertEquals(17.34, $studio->getPayment());

        $studio = $this->studioRepository->findOneBy(['name' => 'Showtime']);
        $this->assertEquals('75aee18236484501b209aa36f95c7e0f', $studio->getId());
        $this->assertEquals(13.45, $studio->getPayment());

        $studio->setName('Showtime Rename');
        $studio->setPayment(15);

        $this->assertEquals('Showtime Rename', $studio->getName());
        $this->assertEquals(15, $studio->getPayment());

        $generator = Factory::create("es_ES");
        $guid = $generator->uuid;
        $name = $generator->company;
        $payment = $generator->randomFloat(2, 10, 20);

        $studio = new Studio($guid);
        $studio->setName($name);
        $studio->setPayment($payment);

        $this->entityManager->persist($studio);
        $this->entityManager->flush();
        $this->assertNotNull($studio->getId());

        $byId = $this->studioRepository->findOneBy(["id" => $studio->getId()]);
        $this->assertEquals($name, $byId->getName());
        $this->assertEquals($payment, $byId->getPayment());
    }
}
