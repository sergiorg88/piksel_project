<?php

namespace App\Tests;

use App\Entity\Studio;
use PHPUnit\Framework\TestCase;

class StudioTest extends TestCase
{
  public function testStudio()
  {
      $studio = new Studio("49924ec6ec6c4efca4aa8b0779c89400");
      $studio->setName("A3 Media");
      $studio->setPayment(11.5);

      $this->assertEquals("49924ec6ec6c4efca4aa8b0779c89400", $studio->getId());
      $this->assertEquals("A3 Media", $studio->getName());
      $this->assertEquals(11.5, $studio->getPayment());
  }
}
