#!/usr/bin/env bash
chmod +x bin/console
composer install -n
bin/console doc:mig:mig --no-interaction
bin/console doc:fix:load --no-interaction
bin/console --env=test doctrine:database:create
bin/console --env=test doctrine:schema:create
echo "Now the enviroment is actived and it is prepared to be used"
echo " "
echo " "
exec "$@"