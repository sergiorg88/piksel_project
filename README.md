## Piksel Challenge Installation Guide

This short guide outlines the steps to be taken in order to set up and test the project.

---

## Project composition

* **api_app** folder with the content of the challenge proposed by Piksel.
* **docker folder** with the contents of the Docker test environment installation.

---

## How to install the project

The project has been set up with Docker, which allows it to run quickly and easily. The only requirement is to have Docker installed on your computer.

Here are the steps to get the project up and running on your PC.

1. Clone the project from the repository.
2. Open the command console from the folder that created the repository when you cloned it and run the following command: **> cd docker/**
3. In the docker folder, launch the following command to perform the installation: **> docker-compose up**.
    * The docker environment will start to install. It will consist of two machines: **One to host the database and one to host the PHP server with Apache2**. This process can take around 4 minutes.
    * The installation itself will execute the following commands to prepare the system:
        * bin/console doctrine:migrations:migrate --no-interaction
        * bin/console doctrine:fixtures:load --no-interaction
        * bin/console --env=test doctrine:database:create
        * bin/console --env=test doctrine:schema:create
    * We will know that the process has finished when the console displays the following message: **Now the enviroment is actived and it is prepared to be used**.
4. Now we can access **localhost:8080** in our browser and if the installation has been successful, the Symfony home screen should be displayed.

---

## Project specifications

The project is structured as follows:

    * 3 Entities:
        * Studio.
        * Episode.
        * RoyaltyManager.

    * 3 Repositories:
        * StudioRepository.
        * EpisodeRepository.
        * RoyaltyManagerRepository.

    * 3 Controllers:
        * StudioController.
        * EpisodeController.
        * RoyaltyManagerController.

This generates an API with the following endpopints:

![API SPEC](md_image/api_spec.jpg)

## Project scope and Tech stack:

The project developed covers the following scope:

    * Automatic data loading using the JSON fixtures delivered with the challenge specifications.
    * API spec with the validations and expected responses indicated in the challenge.
    * Unit testing of the three developed repositories.
    * Functional test of the developed API.

The following technology has been used for this purpose:

    * PHP v8.0.2
    * MariaDB v10.7.3
    * Symfony v6.0.4
    * Doctrine v2.11
    * Composer v2.2.7

---

## How to launch unit testing and functional testing?

To launch the unit tests together with the functional test, you have to access via console to the docker-php machine.
Once inside, you just have to execute the following command: **> php bin/phpunit**.

The files that will be executed when launching the above command can be checked in the **phpunit.xml.dist** file.
These files are located in the **api_app/test** folder.

---

## Environment used for the development of the challenge

The challenge proposed by Piksel has been developed using the following tools and environment. Postman has been used to test the different endpoints and the console from VS Code to launch the unit test suite and functional test.

    * Windows 10
    * VS Code v1.64.2
    * XAMPP v3.3.0:
        * PHP v8.0.2
        * MariaDB v10.4.22
    * Composer v2.2.6
    * Docker-compose v.2.2.3
    * Symfony v6.0.4
    * Doctrine v2.11

---

**This project has been developed by Sergio Rodríguez García to complete the challenge proposed by Piksel.**